﻿using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using DataAccessLayerAbstraction.MarketData.Mapping;
using System;
using System.Collections.Generic;

namespace ChatServiceTests.TestModels
{
    public class DefaultChatServiceTestData
    {
        public CmsUser Booster { get; set; }
        public CmsUser Admin { get; set; }
        public Customer Customer { get; set; }

        public ChatMember ChatMember1 { get; set; }
        public ChatMember ChatMember2 { get; set; }
        public ChatMember ChatMember1Customer { get; set; }

        public ChatChannel ChatChannelAll { get; set; }
        public ChatChannel ChatChannelPrivate { get; set; }
        public ChatChannel ChatChanelLfg { get; set; }
        public ChatChannel ChatChanelOrder { get; set; }

        public DateTime Now { get; set; }
        public DateTime Yesterday { get; set; }
        public DateTime ThreeDaysAgo { get; set; }

        public ChannelChatMessagesTestModel ChatMessagesAll { get; set; }
        public ChannelChatMessagesTestModel ChatMessagesPrivate { get; set; }
        public ChannelChatMessagesTestModel ChatMessagesLfg { get; set; }
        public ChannelChatMessagesTestModel ChatMessagesOrder { get; set; }

        public List<string> NewMessagesId { get; set; }
    }
}
