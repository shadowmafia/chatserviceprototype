﻿using DataAccessLayerAbstraction.ChatData.Mapping;
using System.Collections.Generic;

namespace ChatServiceTests.TestModels
{
    public class ChannelChatMessagesTestModel
    {
        public List<ChatMessage> MessagesForNow { get; set; }
        public List<ChatMessage> MessagesForYesterday { get; set; }
        public List<ChatMessage> MessagesForThreeDaysAgo { get; set; }
    }
}