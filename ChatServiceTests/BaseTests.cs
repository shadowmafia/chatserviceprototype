﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccessLayerAbstraction;
using DataAccessLayerImplementation;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Options;
using Nest;

namespace ChatServiceTests
{
    public class BaseTests
    {
        protected static IDataAccessLayer _dal;
        protected static IElasticClient _elasticClient;
        protected static ElasticDataAccessLayerSettings _elasticDalSettings;
        protected static TestServer _testServer;

        protected static void BaseClassInit()
        {
            _testServer = TestsHelper.InitTestServer();
            _dal = (IDataAccessLayer)_testServer.Services.GetService(typeof(IDataAccessLayer));
            _elasticClient = (IElasticClient)_testServer.Services.GetService(typeof(IElasticClient));
            _elasticDalSettings = ((IOptions<ElasticDataAccessLayerSettings>)_testServer.Services.GetService(typeof(IOptions<ElasticDataAccessLayerSettings>))).Value;
        }

        protected static void BaseClassCleanup()
        {
            _testServer = null;
            _dal = null;
            _elasticClient = null;
            _elasticDalSettings = null;
        }
    }
}
