﻿using System;
using System.Collections.Generic;
using System.Text;
using ChatService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace ChatServiceTests
{
    public static class TestsHelper
    {
        public static TestServer InitTestServer()
        {
            var builder = new WebHostBuilder()
                .UseEnvironment("UnitTest")
                .UseStartup<Startup>();

            return new TestServer(builder);
        }
    }
}
