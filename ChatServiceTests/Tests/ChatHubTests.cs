using ChatServiceTests.TestModels;
using DataAccessLayerAbstraction._CommonEnums.System;
using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using DataAccessLayerAbstraction.MarketData.Mapping;
using DataAccessLayerAbstraction.MarketData.Mapping.CmsUserClasses;
using DataAccessLayerImplementation._ElasticIndexSettings;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetCoreChatClient;
using NetCoreChatClient.Settings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineUserModel = NetCoreChatClient.Models.OnlineUserModel;

namespace ChatServiceTests.Tests
{
    [TestClass]
    public class ChatHubTests : BaseTests
    {
        private static readonly ChatDataIndexSettings settings = _elasticDalSettings.IndexesSettings.ChatDataIndexSettings;

        private static DefaultChatServiceTestData testData;

        public const string HubUrl = "http://localhost:5000/chatHub";

        private static DateTime now = DateTime.Now;
        private static DateTime yesterday = now.AddDays(-1);
        private static DateTime threeDaysAgo = now.AddDays(-3);

        private static object lockObject = new Object();

        [ClassInitialize]
        public static async Task TestClassInit(TestContext testContext)
        {
            BaseClassInit();
            testData = await CreateDefaultTestData();
        }

        [ClassCleanup]
        public static async Task TestClassCleanup()
        {
            await ClearDefaultTestData();
            BaseClassCleanup();
        }

        [TestMethod]
        public async Task TestConnections_OnUserConnect_OnUsersDisconnectEvents()
        {
            var boosterChatClientSettings = CreateDefaultTestChatClientSettings(CreateTokenForCmsUser(testData.Booster));
            ChatHubClient boosterConnection1 = new ChatHubClient(boosterChatClientSettings);
            ChatHubClient boosterConnection2 = new ChatHubClient(boosterChatClientSettings);

            var adminChatClientSettings = CreateDefaultTestChatClientSettings(CreateTokenForCmsUser(testData.Admin));
            ChatHubClient adminConnection1 = new ChatHubClient(adminChatClientSettings);
            ChatHubClient adminConnection2 = new ChatHubClient(adminChatClientSettings);

            var customerChatClientSettings = CreateDefaultTestChatClientSettings(CreateTokenForCustomer(testData.Customer));
            ChatHubClient customerConnection1 = new ChatHubClient(customerChatClientSettings);
            ChatHubClient customerConnection2 = new ChatHubClient(customerChatClientSettings);

            bool isGetFirstDisconnectedEventWithData = false;
            List<OnlineUserModel> usersDisconnected = null;

            int connectedEventsInBoosterConnection1 = 0;
            int connectedEventsInBoosterConnection2 = 0;
            int connectedEventsInCustomerConnection1 = 0;

            boosterConnection1.OnUserConnected += model =>
            {
                connectedEventsInBoosterConnection1++;
            };

            boosterConnection1.OnUsersDisconnected += list =>
            {
                lock (lockObject)
                {
                    if (isGetFirstDisconnectedEventWithData == false)
                    {
                        if (list.Count > 0)
                        {
                            usersDisconnected = list;
                            isGetFirstDisconnectedEventWithData = true;
                        }
                    }
                }
            };

            boosterConnection2.OnUserConnected += model =>
            {
                connectedEventsInBoosterConnection2++;
            };

            customerConnection1.OnUserConnected += model =>
            {
                connectedEventsInCustomerConnection1++;
            };

            await boosterConnection1.Start();
            await boosterConnection2.Start();

            await Task.Delay(100);
            var onlineUserBeforeOthersConnect = await boosterConnection1.GetOnlineUsers();

            await customerConnection1.Start();
            await customerConnection2.Start();
            await adminConnection1.Start();
            await adminConnection2.Start();

            await Task.Delay(200);
            var onlineUserAfterOthersConnect = await boosterConnection1.GetOnlineUsers();

            await customerConnection1.Stop();
            await customerConnection2.Stop();
            await adminConnection1.Stop();
            await adminConnection2.Stop();
            await boosterConnection2.Stop();

            await Task.Delay(100);
            var onlineUserAfterOthersDisconnect = await boosterConnection1.GetOnlineUsers();

            DateTime waitTime = DateTime.UtcNow.AddSeconds(91);

            while (DateTime.UtcNow < waitTime)
            {
                await Task.Delay(1000);
            }

            Assert.AreEqual(2, usersDisconnected.Count);
            Assert.AreEqual(1, onlineUserBeforeOthersConnect.Count);
            Assert.AreEqual(3, onlineUserAfterOthersConnect.Count);
            Assert.AreEqual(1, onlineUserAfterOthersDisconnect.Count);

            Assert.AreEqual(3, connectedEventsInBoosterConnection1);
            Assert.AreEqual(2, connectedEventsInBoosterConnection2);
            Assert.AreEqual(2, connectedEventsInCustomerConnection1);
        }

        [TestMethod]
        public async Task TestGetChannelData()
        {
            ChatHubClient boosterClient =  new ChatHubClient(CreateDefaultTestChatClientSettings(CreateTokenForCmsUser(testData.Booster)));
        }

        [TestMethod]
        public async Task TestUpdateChannelData()
        {

        }


        [TestMethod]
        public async Task TestGetMessageData()
        {

        }

        [TestMethod]
        public async Task TestSendMessage_OnMessageReceivedEvent()
        {

        }

        #region private methods

        private static async Task<DefaultChatServiceTestData> CreateDefaultTestData()
        {
            CmsUser booster = new CmsUser
            {
                CmsUserType = CmsUserType.Booster,
                Name = "TestBooster",
                LastSeen = DateTime.MinValue,
                ProfileImageUrl = "htttps://testImageUrlBooster.png",
            };

            CmsUser admin = new CmsUser
            {
                Id = "adminHuy",
                CmsUserType = CmsUserType.Admin,
                Name = "TestAdmin",
                LastSeen = DateTime.MinValue,
                ProfileImageUrl = "htttps://testImageUrlAdmin.png",
                OnlineStatus = OnlineStatus.Offline
            };

            Customer customer = new Customer
            {
                Username = "TestCustomer",
                AvatarUrl = "htttps://testImageUrlCustomer.png",
                LastSeen = DateTime.MinValue,
            };

            booster.Id = await _dal.MarketData.CmsUser.Manager.AddOrUpdate(booster);
            admin.Id = await _dal.MarketData.CmsUser.Manager.AddOrUpdate(admin);
            customer.Id = await _dal.MarketData.Customer.Manager.AddOrUpdate(customer);

            ChatMember chatMember1 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = booster.Id,
            };

            ChatMember chatMember1Customer = new ChatMember()
            {
                MemberType = ChatMemberType.Customer,
                MemberId = customer.Id
            };

            ChatMember chatMember2 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = admin.Id
            };

            ChatChannel chatChannelAll = new ChatChannel()
            {
                ChannelType = ChatChannelType.All,
                ChannelMembers = new List<ChatMember>()
                {
                    chatMember1,
                    chatMember2,
                    chatMember1Customer
                }
            };

            ChatChannel chatChannelPrivate = new ChatChannel()
            {
                ChannelType = ChatChannelType.Private,
                ChannelMembers = new List<ChatMember>()
                {
                    chatMember1,
                    chatMember2,
                }
            };

            ChatChannel chatChanelLfg = new ChatChannel()
            {
                ChannelType = ChatChannelType.Lfg,
                ChannelMembers = new List<ChatMember>()
                {
                    chatMember1,
                    chatMember2,
                    chatMember1Customer
                }
            };

            ChatChannel chatChanelOrder = new ChatChannel()
            {
                ChannelType = ChatChannelType.Order,
                ChannelMembers = new List<ChatMember>()
                {
                    chatMember1,
                    chatMember2,
                    chatMember1Customer
                }
            };

            chatChannelAll.Id = await _dal.ChatData.Channels.Manager.AddOrUpdate(chatChannelAll);
            chatChannelPrivate.Id = await _dal.ChatData.Channels.Manager.AddOrUpdate(chatChannelPrivate);
            chatChanelLfg.Id = await _dal.ChatData.Channels.Manager.AddOrUpdate(chatChanelLfg);
            chatChanelOrder.Id = await _dal.ChatData.Channels.Manager.AddOrUpdate(chatChanelOrder);

            List<string> mIds = new List<string>
            {
                booster.Id,
                customer.Id,
                admin.Id,
            };

            //int stressTestMessagesCount = 1000000;

            ////Stress test
            //var stressTestMessages = BuildTestMessageDataForChatStressTest(chatChannelAll, mIds, stressTestMessagesCount);
            //var stressTestMessages2 = BuildTestMessageDataForChatStressTest(chatChanelLfg, mIds, 10000000);

            //for (int i = 0; i < 200; i++)
            //{
            //  var x =  await _dal.ChatData.Messages.Manager.CreateOrUpdateChannel(stressTestMessages.messagesForInexing.Skip(i * 5000).Take(5000).ToList());
            //}

            //for (int i = 0; i < 2000 ; i++)
            //{
            //    var x = await _dal.ChatData.Messages.Manager.CreateOrUpdateChannel(stressTestMessages2.messagesForInexing.Skip(i * 5000).Take(5000).ToList());
            //}

            var allMessagesData = BuildTestMessageDataForChat(chatChannelAll, mIds);
            var privateMessagesData = BuildTestMessageDataForChat(chatChannelPrivate, mIds);
            var lfgMessagesData = BuildTestMessageDataForChat(chatChanelLfg, mIds);
            var orderMessagesData = BuildTestMessageDataForChat(chatChanelOrder, mIds);

            chatChannelAll.LastMessage = allMessagesData.lastChannelMessage;
            chatChannelPrivate.LastMessage = privateMessagesData.lastChannelMessage;
            chatChanelLfg.LastMessage = lfgMessagesData.lastChannelMessage;
            chatChanelOrder.LastMessage = orderMessagesData.lastChannelMessage;

            List<ChatMessage> messagesForIndexing = new List<ChatMessage>();

            messagesForIndexing.AddRange(allMessagesData.messagesForInexing);
            messagesForIndexing.AddRange(privateMessagesData.messagesForInexing);
            messagesForIndexing.AddRange(lfgMessagesData.messagesForInexing);
            messagesForIndexing.AddRange(orderMessagesData.messagesForInexing);

            List<string> newMessagesId = await _dal.ChatData.Messages.Manager.AddOrUpdate(messagesForIndexing);

            await _elasticClient.Indices.RefreshAsync(settings.ChatMessageIndexSettings.ActualMessagesAliasName);
            await _elasticClient.Indices.RefreshAsync(settings.ChatMessageIndexSettings.CurrentMonthMessagesAliasName);
            await _elasticClient.Indices.RefreshAsync(settings.ChatChannelIndexName);

            return new DefaultChatServiceTestData
            {
                Booster = booster,
                Admin = admin,
                Customer = customer,

                ChatMember1 = chatMember1,
                ChatMember2 = chatMember2,
                ChatMember1Customer = chatMember1Customer,

                Now = now,
                Yesterday = yesterday,
                ThreeDaysAgo = threeDaysAgo,

                ChatMessagesAll = allMessagesData.messgesTestChatModel,
                ChatMessagesLfg = lfgMessagesData.messgesTestChatModel,
                ChatMessagesOrder = orderMessagesData.messgesTestChatModel,
                ChatMessagesPrivate = privateMessagesData.messgesTestChatModel,

                NewMessagesId = newMessagesId,

                ChatChanelLfg = chatChanelLfg,
                ChatChanelOrder = chatChanelOrder,
                ChatChannelAll = chatChannelAll,
                ChatChannelPrivate = chatChannelPrivate
            };
        }

        private static async Task ClearDefaultTestData()
        {
            await _elasticClient.Indices.DeleteAsync(settings.ChatChannelIndexName);
            await _elasticClient.Indices.DeleteAsync(
                $"{_elasticDalSettings.IndexesSettings.ChatDataIndexSettings.ChatMessageIndexSettings.ChatMessageIndexName}_{DateTime.UtcNow.Year}_{DateTime.UtcNow:MM}"
            );
            await _elasticClient.Indices.DeleteAsync(_elasticDalSettings.IndexesSettings.MarketDataIndexSettings.CmsUserIndexName);
            await _elasticClient.Indices.DeleteAsync(_elasticDalSettings.IndexesSettings.MarketDataIndexSettings.CustomerIndexName);
        }

        private static List<ChatMessage> GetMessagesPackByDate(string channelId, DateTime time, List<string> memberIds)
        {
            var chatMember1 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = memberIds[0],
            };
            var chatMember1Customer = new ChatMember()
            {
                MemberType = ChatMemberType.Customer,
                MemberId = memberIds[1]
            };
            var chatMember2 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = memberIds[2]
            };


            ChatMessage channel_Message1 = new ChatMessage()
            {
                ChannelId = channelId,
                Content = "Hello my  dear friend",
                Sender = chatMember1,
                Timestamp = time.AddHours(-1)
            };

            ChatMessage channel_Message2 = new ChatMessage()
            {
                ChannelId = channelId,
                Content = "Hello my  dear friend2",
                Sender = chatMember1,
                Timestamp = time.AddMinutes(-2)
            };

            ChatMessage channel_Message3 = new ChatMessage()
            {
                ChannelId = channelId,
                Content = "Hello my  dear friend3",
                Sender = chatMember2,
                Timestamp = time.AddMinutes(-3)
            };

            List<ChatMessage> resultList = new List<ChatMessage>
            {
                channel_Message1,
                channel_Message2,
                channel_Message3
            };

            return resultList;
        }

        private List<ChatMessage> GetMessagesPackByDateStressTest(string channelId, DateTime time, List<string> memberIds, int iterationsCount)
        {
            var chatMember1 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = memberIds[0],
            };
            var chatMember1Customer = new ChatMember()
            {
                MemberType = ChatMemberType.Customer,
                MemberId = memberIds[1]
            };
            var chatMember2 = new ChatMember()
            {
                MemberType = ChatMemberType.CmsUser,
                MemberId = memberIds[2]
            };

            List<ChatMessage> resultList = new List<ChatMessage>(iterationsCount * 3);

            for (int i = 0; i < iterationsCount; i++)
            {
                ChatMessage channel_Message1 = new ChatMessage()
                {
                    ChannelId = channelId,
                    Content = "Hello my  dear friend",
                    Sender = chatMember1,
                    Timestamp = time.AddHours(-1).AddMinutes(i)
                };

                ChatMessage channel_Message2 = new ChatMessage()
                {
                    ChannelId = channelId,
                    Content = "Hello my  dear friend2",
                    Sender = chatMember1,
                    Timestamp = time.AddHours(-2).AddMinutes(i - 1)
                };

                ChatMessage channel_Message3 = new ChatMessage()
                {
                    ChannelId = channelId,
                    Content = "Hello my  dear friend3",
                    Sender = chatMember2,
                    Timestamp = time.AddHours(-3).AddMinutes(i - 2)
                };

                resultList.AddRange(new List<ChatMessage>
                {
                    channel_Message1,
                    channel_Message2,
                    channel_Message3
                });
            }



            return resultList;
        }

        private static (ChatMessage lastChannelMessage, ChannelChatMessagesTestModel messgesTestChatModel, List<ChatMessage> messagesForInexing) BuildTestMessageDataForChat(ChatChannel channel, List<string> mIds)
        {
            var chatMessages = new List<ChatMessage>();

            ChannelChatMessagesTestModel channelMessages = new ChannelChatMessagesTestModel();

            channelMessages.MessagesForNow = GetMessagesPackByDate(channel.Id, now, mIds);
            channelMessages.MessagesForYesterday = GetMessagesPackByDate(channel.Id, yesterday, mIds);
            channelMessages.MessagesForThreeDaysAgo = GetMessagesPackByDate(channel.Id, threeDaysAgo, mIds);

            chatMessages.AddRange(channelMessages.MessagesForNow);
            chatMessages.AddRange(channelMessages.MessagesForYesterday);
            chatMessages.AddRange(channelMessages.MessagesForThreeDaysAgo);

            return (lastChannelMessage: channelMessages.MessagesForNow[0], messgesTestChatModel: channelMessages, messagesForInexing: chatMessages);
        }
        //GetMessagesPackByDateStressTest

        private (ChatMessage lastChannelMessage, ChannelChatMessagesTestModel messgesTestChatModel, List<ChatMessage> messagesForInexing) BuildTestMessageDataForChatStressTest(ChatChannel channel, List<string> mIds, int messagesCount)
        {
            var chatMessages = new List<ChatMessage>();

            ChannelChatMessagesTestModel channelMessages = new ChannelChatMessagesTestModel();

            channelMessages.MessagesForNow = GetMessagesPackByDateStressTest(channel.Id, now, mIds, messagesCount / 3 / 3);
            channelMessages.MessagesForYesterday = GetMessagesPackByDateStressTest(channel.Id, yesterday, mIds, messagesCount / 3 / 3);
            channelMessages.MessagesForThreeDaysAgo = GetMessagesPackByDateStressTest(channel.Id, threeDaysAgo, mIds, messagesCount / 3 / 3);

            chatMessages.AddRange(channelMessages.MessagesForNow);
            chatMessages.AddRange(channelMessages.MessagesForYesterday);
            chatMessages.AddRange(channelMessages.MessagesForThreeDaysAgo);

            return (lastChannelMessage: channelMessages.MessagesForNow[0], messgesTestChatModel: channelMessages, messagesForInexing: chatMessages);
        }

        private ChatHubClientSettings CreateDefaultTestChatClientSettings(string authToken)
        {
            return new ChatHubClientSettings
            {
                AuthToken = authToken,
                HubUrl = HubUrl,
                MaxContentLength = 500,
                MaxTakeChannelsCount = 40,
                MaxTakeMessagesCount = 40,
                MinContentLength = 1
            };
        }

        private string CreateTokenForCmsUser(CmsUser user)
        {
            return $"{user.Id}</separator>{user.CmsUserType}";
        }

        private string CreateTokenForCustomer(Customer user)
        {
            return $"{user.Id}</separator>Customer";
        }

        #endregion
    }

    enum UserRoleTmp
    {
        System = 0,
        SuperAdmin = 1,
        Admin = 2,
        Manager = 3,
        Booster = 4,
        Customer = 5
    }
}
