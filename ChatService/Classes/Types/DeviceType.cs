﻿namespace ChatService.Classes.Types
{
    public enum DeviceType
    {
        Web,
        Desktop,
        Mobile
    }
}
