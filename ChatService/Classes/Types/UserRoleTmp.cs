﻿namespace ChatService.Classes.Types
{
    public enum UserRoleTmp
    {
        System = 0,
        SuperAdmin = 1,
        Admin = 2,
        Manager = 3,
        Booster = 4,
        Customer = 5
    }
}
