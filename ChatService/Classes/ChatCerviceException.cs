﻿using System;

namespace ChatService.Classes
{
    public class ChatServiceException : Exception
    {
        public bool IsPublic { get; }
        public ChatServiceException(string message, bool isPublicException = false) : base(message)
        {
            IsPublic = isPublicException;
        }
    }
}
