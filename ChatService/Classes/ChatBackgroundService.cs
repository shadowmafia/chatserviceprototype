﻿using ChatService.Classes.Types;
using ChatService.Hubs;
using ChatService.Models.ChatData;
using ChatService.Models.ChatSystem;
using ChatService.Models.Settings;
using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using DataAccessLayerAbstraction.MarketData.Mapping;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ChatService.Classes
{
    public class ChatBackgroundService
    {
        private readonly SemaphoreSlim _semaphore;

        private readonly IDataAccessLayer _dal;
        private readonly IHubContext<ChatHub, IChatClient> _chatHubContext;

        /// <summary>
        /// Key is user userId plus memberType.
        /// </summary>
        private readonly ConcurrentDictionary<string, UserConnections> _userConnections;
        /// <summary>
        /// Key is channel Id.
        /// </summary>
        private readonly ConcurrentDictionary<string, ChatChannel> _channels;

        private readonly ConcurrentDictionary<string, UserForDelayedDisconnect> _usersForDisconnectBuffer;
        private Timer _sendDisconnectTimer;

        private readonly ChatBackgroundServiceSettings _settings;

        public ChatBackgroundService(
            IDataAccessLayer dal,
            IHubContext<ChatHub, IChatClient> hubContext,
            IOptions<ChatServiceSettings> settings)
        {
            _dal = dal ?? throw new ArgumentException(nameof(dal));
            _chatHubContext = hubContext ?? throw new ArgumentException(nameof(hubContext));

            _settings = settings.Value.ChatBackgroundServiceSettings ?? throw new ArgumentException(nameof(settings.Value.ChatBackgroundServiceSettings));

            _settings.DisconnectNotificationPeriodInSeconds = _settings.DisconnectNotificationPeriodInSeconds > 0
                ? _settings.DisconnectNotificationPeriodInSeconds
                : throw new ArgumentOutOfRangeException(nameof(_settings.DisconnectNotificationPeriodInSeconds));

            _semaphore = new SemaphoreSlim(1,1);
            _userConnections = new ConcurrentDictionary<string, UserConnections>();
            _channels = new ConcurrentDictionary<string, ChatChannel>();
            _usersForDisconnectBuffer = new ConcurrentDictionary<string, UserForDelayedDisconnect>();

            _sendDisconnectTimer = new Timer(SendDisconnections, null, TimeSpan.Zero, TimeSpan.FromSeconds(_settings.DisconnectNotificationPeriodInSeconds));
        }

        public async Task AddUserConnection(ChatSub chatSub, string connectionId, DeviceType device)
        {
            if (string.IsNullOrEmpty(chatSub.Member.MemberId))
            {
                throw new ArgumentNullException(chatSub.Member.MemberId);
            }

            if (string.IsNullOrEmpty(connectionId))
            {
                throw new ArgumentNullException(connectionId);
            }

            ChatMemberModel currentMember = new ChatMemberModel();

            if (chatSub.RoleInSystem == UserRoleTmp.Customer)
            {
                Customer customerData = await _dal.MarketData.Customer.Searcher.GetCustomer(chatSub.Member.MemberId)
                                        ?? throw new ChatServiceException($"Customer with channelId: {chatSub.Member.MemberId} not fond in db.");

                currentMember.MemberId = customerData.Id;
                currentMember.AvatarUrl = customerData.AvatarUrl;
                currentMember.MemberType = ChatMemberType.Customer;
                currentMember.NickName = customerData.Username;
            }
            else
            {
                CmsUser cmsUserData = await _dal.MarketData.CmsUser.Searcher.GetCmsUser(chatSub.Member.MemberId)
                                      ?? throw new ChatServiceException($"CmsUser with channelId: {chatSub.Member.MemberId} not fond in db.");

                currentMember.MemberId = cmsUserData.Id;
                currentMember.AvatarUrl = cmsUserData.ProfileImageUrl;
                currentMember.MemberType = ChatMemberType.CmsUser;
                currentMember.NickName = cmsUserData.Name;
                currentMember.CmsUserType = cmsUserData.CmsUserType;
            }

            string userKey = GetConnectionKey(currentMember.MemberId, currentMember.MemberType);

            bool isFirstDevice = true;

            await  _semaphore.WaitAsync();

            if (_userConnections.TryGetValue(userKey, out UserConnections connection))
            {
                isFirstDevice = false;

                UserConnections newModel = new UserConnections
                {
                    User = currentMember,
                    Connections = connection.Connections
                };

                newModel.Connections.Add(new Connection
                {
                    ConnectionId = connectionId,
                    Device = device
                });

                _userConnections[userKey] = newModel;
            }
            else
            {
                _userConnections.TryAdd(userKey, new UserConnections
                {
                    User = currentMember,
                    Connections = new List<Connection>
                    {
                        new Connection
                        {
                            ConnectionId = connectionId,
                            Device = device,
                            ConnectionTime = DateTime.UtcNow
                        }
                    }
                });
            }

            if (_usersForDisconnectBuffer.ContainsKey(userKey))
            {
                if (isFirstDevice && _usersForDisconnectBuffer[userKey].DisconnectTime < DateTime.UtcNow.AddSeconds(-_settings.DisconnectNotificationPeriodInSeconds))
                {
                    var user = _userConnections[GetConnectionKey(chatSub.Member.MemberId, chatSub.Member.MemberType)];
                    await _chatHubContext.Clients.All.UserConnected(MapUserConnectionToOnlineUserModel(user));
                }

                _usersForDisconnectBuffer.TryRemove(userKey, out _);
            }
            else
            {
                if (isFirstDevice)
                {
                    var user = _userConnections[userKey];
                    await _chatHubContext.Clients.All.UserConnected(MapUserConnectionToOnlineUserModel(user));
                }
            }

            _semaphore.Release();
        }

        public void RemoveUserConnection(ChatSub chatSub, string connectionId)
        {
            if (string.IsNullOrEmpty(chatSub.Member.MemberId))
            {
                throw new ArgumentNullException(chatSub.Member.MemberId);
            }

            if (string.IsNullOrEmpty(connectionId))
            {
                throw new ArgumentNullException(connectionId);
            }

            ChatMember currentMember = chatSub.Member;

            string userKey = GetConnectionKey(currentMember.MemberId, currentMember.MemberType);

            _semaphore.Wait();

            UserConnections currentUserConnections = _userConnections[userKey];

            UserConnections newModel = new UserConnections
            {
                User = currentUserConnections.User,
                Connections = currentUserConnections.Connections
            };

            newModel.Connections.RemoveAll(_ => _.ConnectionId == connectionId);

            bool isLastDevice = false;

            if (!newModel.Connections.Any())
            {
                _userConnections.Remove(userKey, out _);
                isLastDevice = true;
            }
            else
            {
                _userConnections[userKey] = newModel;
            }

            if (isLastDevice)
            {
                _usersForDisconnectBuffer.TryAdd(userKey, new UserForDelayedDisconnect
                {
                    User = currentUserConnections.User,
                    DisconnectTime = DateTime.UtcNow
                });
            }

            _semaphore.Release();
        }

        public async Task<ChatUserChannelsModel> GetChannelsInfo(ChatSub chatSub, int? channelsLoadingCount = null)
        {
            if (string.IsNullOrEmpty(chatSub.Member.MemberId))
            {
                throw new ArgumentNullException(nameof(chatSub.Member.MemberId));
            }

            IList<ChatChannel> allChatMemberChannels = await _dal.ChatData.Channels.Searcher.GetChannelsSummary(
                chatSub.Member,
                channelsLoadingCount,
                channelsLoadingCount
            );

            var mappedChatChannels = await BuildChatChannelModels(allChatMemberChannels, chatSub.Member);

            ChatUserChannelsModel chatUserChanelModel = new ChatUserChannelsModel()
            {
                Channels = mappedChatChannels.mappedChannels,
                TotalUnreadMessagesCount = mappedChatChannels.totalUnreadMessagesCount
            };

            return chatUserChanelModel;
        }

        public async Task<List<ChatChannelModel>> GetChannelsInfoByType(ChatSub chatSub, ChatChannelType type, DateTime? elderThen = null, int? count = null)
        {
            if (string.IsNullOrEmpty(chatSub.Member.MemberId))
            {
                throw new ArgumentNullException(nameof(chatSub.Member.MemberId));
            }

            IList<ChatChannel> channels = await _dal.ChatData.Channels.Searcher.GetChannelsByMember(
               member: chatSub.Member,
               channelTypes: new List<ChatChannelType> { type },
               elderThen: elderThen,
               take: count
            );

            return (await BuildChatChannelModels(channels, chatSub.Member)).mappedChannels;
        }

        public async Task<ChatChannelModel> GetChannelInfo(ChatSub chatSub, string channelId)
        {
            if (string.IsNullOrEmpty(chatSub.Member.MemberId))
            {
                throw new ArgumentNullException(nameof(chatSub.Member.MemberId));
            }

            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            ChatChannel channel = await _dal.ChatData.Channels.Searcher.GetChannel(channelId);

            if (channel == null)
            {
                throw new ChatServiceException($"Channel not found channel channelId:{channelId}.");
            }

            CheckAccessToChannel(chatSub, channel);

            return (
                await BuildChatChannelModels(
                    new List<ChatChannel> { channel },
                    chatSub.Member
                )).mappedChannels.First();
        }

        public async Task<List<ChatMessageModel>> GetMessagesFromChannel(ChatSub chatSub, string channelId, DateTime? elderThen = null, int? count = null)
        {
            if (string.IsNullOrEmpty(chatSub.Member.MemberId))
            {
                throw new ArgumentNullException(nameof(chatSub.Member.MemberId));
            }

            ChatChannel currentChannel = await _dal.ChatData.Channels.Searcher.GetChannel(channelId);

            CheckAccessToChannel(chatSub, currentChannel);

            IList<ChatMessage> messages = await _dal.ChatData.Messages.Searcher.FindMessages(currentChannel.Id, elderThen: elderThen, take: count);

            return await BuildChatMessageModels(messages);
        }

        public async Task<ChatChannelModel> GetPrivateChannelWithMember(ChatSub chatSub, ChatMember receiver)
        {
            if (string.IsNullOrEmpty(chatSub.Member.MemberId))
            {
                throw new ArgumentNullException(nameof(chatSub.Member.MemberId));
            }

            if (string.IsNullOrEmpty(receiver.MemberId))
            {
                throw new ArgumentNullException(nameof(receiver.MemberId));
            }

            ChatMember currentMember = chatSub.Member;

            ChatChannel currentChannel = _channels.Values.FirstOrDefault(_ =>
                _.ChannelType == ChatChannelType.Private &&
                _.ChannelMembers.Any(__ => __.MemberId == currentMember.MemberId && __.MemberType == currentMember.MemberType) &&
                _.ChannelMembers.Any(__ => __.MemberId == receiver.MemberId && __.MemberType == receiver.MemberType)
            );

            if (currentChannel == null)
            {
                currentChannel = await _dal.ChatData.Channels.Searcher.FindPrivateChannelBetweenUsers(currentMember, receiver);
            }

            return (await BuildChatChannelModels(new List<ChatChannel>() { currentChannel })).mappedChannels.First();
        }

        public async Task<ChatChannelModel> CreatePrivateChannelWithMember(ChatSub chatSub, ChatMember receiver)
        {
            if (string.IsNullOrEmpty(chatSub.Member.MemberId))
            {
                throw new ArgumentNullException(nameof(receiver.MemberId));
            }

            if (string.IsNullOrEmpty(receiver.MemberId))
            {
                throw new ArgumentNullException(nameof(receiver.MemberId));
            }

            ChatMember currentMember = chatSub.Member;

            ChatChannel currentChannel = _channels.Values.FirstOrDefault(_ =>
                _.ChannelType == ChatChannelType.Private &&
                _.ChannelMembers.Any(__ => __.MemberId == currentMember.MemberId && __.MemberType == currentMember.MemberType) &&
                _.ChannelMembers.Any(__ => __.MemberId == receiver.MemberId && __.MemberType == receiver.MemberType)
            );

            if (currentChannel == null)
            {
                currentChannel = await _dal.ChatData.Channels.Searcher.FindPrivateChannelBetweenUsers(currentMember, receiver);

                if (currentChannel == null)
                {
                    var newChannel = new ChatChannel
                    {
                        ChannelType = ChatChannelType.Private,
                        ChannelMembers = new List<ChatMember>
                        {
                            currentMember,
                            receiver
                        }
                    };

                    newChannel.Id = await _dal.ChatData.Channels.Manager.AddOrUpdate(newChannel);
       
                    return (await BuildChatChannelModels(new List<ChatChannel>() { newChannel })).mappedChannels.First();
                }
            }

            throw new ChatServiceException($"Channel between users already exist. user {currentMember.MemberId}_{currentMember.MemberType} receiver {receiver.MemberId}_{receiver.MemberType}",true);
        }

        public List<OnlineUserModel> GetOnlineUsers()
        {
            var usersList = _userConnections.Values.GroupBy(_ => _.User.MemberId).Select(grp => grp.First()).ToList();
            List<OnlineUserModel> list = new List<OnlineUserModel>(usersList.Count);

            foreach (var user in usersList)
            {
                list.Add(MapUserConnectionToOnlineUserModel(user));
            }

            return list;
        }

        public async Task<ChatMessageModel> SendMessage(ChatSub chatSub, string channelId, string messageMarker, string content, string attachedImgUrl)
        {
            if (string.IsNullOrEmpty(chatSub.Member.MemberId))
            {
                throw new ArgumentNullException(nameof(chatSub.Member.MemberId));
            }

            ChatMember currentMember = chatSub.Member;
            ChatChannel currentChannel = await GetOrUploadChatChannel(channelId);

            if (currentChannel == null)
            {
                throw new ChatServiceException($"Channel not found channel channelId:{channelId}.");
            }

            CheckAccessToChannel(chatSub, currentChannel);

            ChatMessage newMessage = new ChatMessage
            {
                ChannelId = channelId,
                Sender = currentMember,
                AttachedImageUrl = attachedImgUrl,
                Content = content,
                Timestamp = DateTime.UtcNow,
                MessageMarker = messageMarker
            };

            newMessage.Id = await _dal.ChatData.Messages.Manager.AddOrUpdate(newMessage);
            await _dal.ChatData.Channels.Manager.UpdateLastMassage(channelId, newMessage);

            return (await BuildChatMessageModels(new List<ChatMessage>
            {
                newMessage
            })).FirstOrDefault();
        }

        public async Task JoinToChannel(ChatSub chatSub, string channelId)
        {
            if (string.IsNullOrEmpty(chatSub.Member.MemberId))
            {
                throw new ArgumentNullException(nameof(chatSub.Member.MemberId));
            }

            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(channelId);
            }

            ChatMember currentMember = chatSub.Member;
            ChatChannel currentChannel = await GetOrUploadChatChannel(channelId);

            if (currentChannel == null)
            {
                throw new ChatServiceException($"Channel not found channel channelId:{channelId}.");
            }

            CheckAccessToChannel(chatSub, currentChannel);

            if (currentChannel.ChannelMembers.Any(_ => _.MemberId == currentMember.MemberId && _.MemberType == currentMember.MemberType))
            {
                throw new ChatServiceException($"User:{currentMember.MemberId}_{currentMember.MemberType} already joined to channel:{channelId}", true);
            }

            _channels[channelId].ChannelMembers.Add(currentMember);
            await _dal.ChatData.Channels.Manager.AddMember(channelId, currentMember);
        }

        public async Task LeaveFromChannel(ChatSub chatSub, string channelId)
        {
            if (string.IsNullOrEmpty(chatSub.Member.MemberId))
            {
                throw new ArgumentNullException(nameof(chatSub.Member.MemberId));
            }

            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(channelId);
            }

            ChatMember currentMember = chatSub.Member;
            ChatChannel currentChannel = await GetOrUploadChatChannel(channelId);

            if (currentChannel == null)
            {
                throw new ChatServiceException($"Channel not found channel channelId:{channelId}.");
            }

            if (currentChannel.ChannelType == ChatChannelType.Private)
            {
                throw new ChatServiceException($"Leave is not available for private channels channel channelId:{channelId}. user:{currentMember.MemberId}_{currentMember.MemberType}",true);
            }

            if (currentChannel.ChannelType == ChatChannelType.Order)
            {
                if (chatSub.RoleInSystem == UserRoleTmp.Customer || chatSub.RoleInSystem == UserRoleTmp.Booster)
                {
                    throw new ChatServiceException($"Leave available for order channels only for {UserRoleTmp.Manager} or above. channel channelId:{channelId}. user:{currentMember.MemberId}_{currentMember.MemberType}", true);
                }
            }

            if (currentChannel.ChannelMembers.Any(_ => _.MemberId == currentMember.MemberId && _.MemberType == currentMember.MemberType) == false)
            {
                throw new ChatServiceException($"User:{currentMember.MemberId}_{currentMember.MemberType} not found in channel:{channelId}", true);
            }

            _channels[channelId].ChannelMembers.RemoveAll(_ => _.MemberId == currentMember.MemberId && _.MemberType == currentMember.MemberType);
            await _dal.ChatData.Channels.Manager.RemoveMember(channelId, currentMember);
        }

        //System methods
        public async Task CreateOrUpdateChannel(ChatChannel channel)
        {
            await _dal.ChatData.Channels.Manager.AddOrUpdate(channel);

            if (string.IsNullOrEmpty(channel.Id) == false && _channels.TryGetValue(channel.Id, out ChatChannel oldChanel))
            {
                _channels[channel.Id] = channel;
            }
        }

        public async Task AddMemberToChannel(string channelId, ChatMember member)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            if (string.IsNullOrEmpty(member.MemberId))
            {
                throw new ArgumentNullException(nameof(member.MemberId));
            }

            ChatChannel currentChannel = await _dal.ChatData.Channels.Searcher.GetChannel(channelId);

            if (currentChannel == null)
            {
                throw new ChatServiceException($"Channel not found channel channelId:{channelId}.");
            }

            if (currentChannel.ChannelMembers.Any(_ => _.MemberId == member.MemberId && _.MemberType == member.MemberType))
            {
                throw new ChatServiceException($"User:{member.MemberId}_{member.MemberType} already joined to channel:{channelId}", true);
            }

            await _dal.ChatData.Channels.Manager.AddMember(channelId, member);

            if (_channels.ContainsKey(channelId))
            {
                _channels[channelId].ChannelMembers.Add(member);
            }
        }

        public async Task RemoveMemberFromChannel(string channelId, ChatMember member)
        {
            if (string.IsNullOrEmpty(channelId))
            {
                throw new ArgumentNullException(nameof(channelId));
            }

            if (string.IsNullOrEmpty(member.MemberId))
            {
                throw new ArgumentNullException(nameof(member.MemberId));
            }

            ChatChannel currentChannel = await _dal.ChatData.Channels.Searcher.GetChannel(channelId);

            if (currentChannel == null)
            {
                throw new ChatServiceException($"Channel not found channel channelId:{channelId}.");
            }

            if (currentChannel.ChannelMembers.Any(_ => _.MemberId == member.MemberId && _.MemberType == member.MemberType) == false)
            {
                throw new ChatServiceException($"User:{member.MemberId}_{member.MemberType} not found in channel:{channelId}", true);
            }

            await _dal.ChatData.Channels.Manager.RemoveMember(channelId, member);

            if (_channels.ContainsKey(channelId))
            {
                _channels[channelId].ChannelMembers.RemoveAll(_ => _.MemberId == member.MemberId && _.MemberType == member.MemberType);
            }
        }

        #region private methods

        private async void SendDisconnections(object state)
        {
            List<OnlineUserModel> disconnectedUsers = new List<OnlineUserModel>();

            foreach (var user in _usersForDisconnectBuffer)
            {
                if (user.Value.DisconnectTime < DateTime.UtcNow.AddSeconds(- _settings.DisconnectNotificationPeriodInSeconds))
                {
                    disconnectedUsers.Add(new OnlineUserModel
                    {
                        MemberId = user.Value.User.MemberId,
                        MemberType = user.Value.User.MemberType,
                        CmsUserType = user.Value.User.CmsUserType,
                        AvatarUrl = user.Value.User.AvatarUrl,
                        NickName = user.Value.User.NickName
                    });

                    _usersForDisconnectBuffer.Remove(user.Key, out _);
                }
            }

            await _chatHubContext.Clients.All.UsersDisconnected(disconnectedUsers);
        }

        //Work with hub data
        private async Task<ChatChannel> GetOrUploadChatChannel(string channelId)
        {
            if (_channels.ContainsKey(channelId))
            {
                return _channels[channelId];
            }

            ChatChannel currentChannel = await _dal.ChatData.Channels.Searcher.GetChannel(channelId);
            _channels.TryAdd(currentChannel.Id, currentChannel);

            return currentChannel;
        }

        private List<string> GetUsersConnectionsFromChannel(ChatChannel channel)
        {
            List<string> resultList = new List<string>();

            foreach (var member in channel.ChannelMembers)
            {
                string userConnectionKey = GetConnectionKey(member.MemberId, member.MemberType);

                if (_userConnections.ContainsKey(userConnectionKey))
                {
                    foreach (var connection in _userConnections[userConnectionKey].Connections)
                    {
                        resultList.Add(connection.ConnectionId);
                    }
                }
            }

            return resultList;
        }

        private void CheckAccessToChannel(ChatSub chatSub, ChatChannel currentChannel)
        {
            if (chatSub.RoleInSystem == UserRoleTmp.SuperAdmin || chatSub.RoleInSystem == UserRoleTmp.System)
            {
                return;
            }

            if (currentChannel.ChannelType == ChatChannelType.Private)
            {
                if (currentChannel.ChannelMembers.Any(_ => _.MemberId == chatSub.Member.MemberId && _.MemberType == chatSub.Member.MemberType) == false)
                {
                    throw new ChatServiceException($"Trying to access someone else 's channel channel channelId : {currentChannel.Id}, member channelId:{chatSub.Member.MemberId}_{chatSub.Member.MemberType}");
                }
            }

            if (currentChannel.ChannelType == ChatChannelType.Order)
            {
                if (chatSub.RoleInSystem == UserRoleTmp.Customer || chatSub.RoleInSystem == UserRoleTmp.Booster)
                {
                    bool isHaveAccess = currentChannel.ChannelMembers.Any(_ => _.MemberId == chatSub.Member.MemberId && _.MemberType == chatSub.Member.MemberType);

                    if (isHaveAccess == false)
                    {
                        throw new ChatServiceException($"Invalid access level to channel userId: {chatSub.Member.MemberId} , channel :{currentChannel.Id}");
                    }
                }
            }
        }

        //Build models
        private string GetConnectionKey(string id, ChatMemberType type)
        {
            return $"{id}{type}";
        }

        private async Task<(List<ChatChannelModel> mappedChannels, long totalUnreadMessagesCount)> BuildChatChannelModels(
            IList<ChatChannel> allChatMemberChannels,
            ChatMember chatMemberForCalcUnreadMessages = null)
        {
            IList<(string chanelId, long unreadMessagesCount)> allChatsUnreadMessages = await _dal.ChatData.Messages.Searcher.GetUnreadMessagesInChannels(
             chatMemberForCalcUnreadMessages
           );

            HashSet<string> allNeededCmsUserMembers = new HashSet<string>();
            HashSet<string> allNeededCustomerMembers = new HashSet<string>();

            foreach (var channel in allChatMemberChannels)
            {
                if (channel.LastMessage != null)
                {
                    if (channel.LastMessage.Sender.MemberType == ChatMemberType.CmsUser)
                    {
                        allNeededCmsUserMembers.Add(channel.LastMessage.Sender.MemberId);
                    }
                    else
                    {
                        allNeededCustomerMembers.Add(channel.LastMessage.Sender.MemberId);
                    }
                }

                foreach (var member in channel.ChannelMembers)
                {
                    if (member.MemberType == ChatMemberType.CmsUser)
                    {
                        allNeededCmsUserMembers.Add(member.MemberId);
                    }
                    else
                    {
                        allNeededCustomerMembers.Add(member.MemberId);
                    }
                }
            }

            IList<CmsUser> cmsUser = await _dal.MarketData.CmsUser.Searcher.FindCmsUsers(
                ids: allNeededCmsUserMembers);
            IList<Customer> customers = await _dal.MarketData.Customer.Searcher.FindCustomers(
                ids: allNeededCustomerMembers);

            Dictionary<string, ChatMemberModel> allNeededMemberModels = new Dictionary<string, ChatMemberModel>(cmsUser.Count + customers.Count);

            foreach (var item in cmsUser)
            {
                allNeededMemberModels.Add($"{item.Id}_{ChatMemberType.CmsUser}", new ChatMemberModel
                {
                    MemberId = item.Id,
                    MemberType = ChatMemberType.CmsUser,
                    AvatarUrl = item.ProfileImageUrl,
                    NickName = item.Name
                });
            }

            foreach (var item in customers)
            {
                allNeededMemberModels.Add($"{item.Id}_{ChatMemberType.Customer}", new ChatMemberModel
                {
                    MemberId = item.Id,
                    MemberType = ChatMemberType.CmsUser,
                    AvatarUrl = item.AvatarUrl,
                    NickName = item.Username
                });
            }

            List<ChatChannelModel> mappedChatChannels = new List<ChatChannelModel>(allChatMemberChannels.Count);

            foreach (ChatChannel item in allChatMemberChannels)
            {
                ChatMemberModel sender = null;
                ChatMessageModel lastMessage = null;

                if (item.LastMessage != null)
                {
                    var userKey = $"{item.LastMessage.Sender.MemberId}_{item.LastMessage.Sender.MemberType}";
                    sender = allNeededMemberModels[userKey];

                    lastMessage = new ChatMessageModel
                    {
                        Id = item.LastMessage.Id,
                        ChannelId = item.LastMessage.ChannelId,
                        Sender = sender,
                        Timestamp = item.LastMessage.Timestamp,
                        Content = item.LastMessage.Content,
                        AttachedImageUrl = item.LastMessage.AttachedImageUrl
                    };
                }

                List<ChatMemberModel> channelMembers = new List<ChatMemberModel>(item.ChannelMembers?.Count ?? 0);

                if (item.ChannelMembers != null)
                {
                    foreach (var member in item.ChannelMembers)
                    {
                        channelMembers.Add(allNeededMemberModels[$"{member.MemberId}_{member.MemberType}"]);
                    }
                }

                mappedChatChannels.Add(
                    new ChatChannelModel
                    {
                        Id = item.Id,
                        LastMessage = lastMessage,
                        ChannelMembers = channelMembers,
                        LinkedObjectId = item.LinkedObjectId,
                        ChannelType = item.ChannelType
                    }
                );
            }

            long totalUnreadMessagesCount = 0;

            foreach ((string chanelId, long unreadMessagesCount) currentChatUnreadMessage in allChatsUnreadMessages)
            {
                var currentChatChanel = mappedChatChannels.FirstOrDefault(_ => _.Id == currentChatUnreadMessage.chanelId);

                if (currentChatChanel != null)
                {
                    currentChatChanel.UnreadMessagesCount = currentChatUnreadMessage.unreadMessagesCount;
                }

                totalUnreadMessagesCount += currentChatUnreadMessage.unreadMessagesCount;
            }

            return (mappedChannels: mappedChatChannels, totalUnreadMessagesCount: totalUnreadMessagesCount);
        }

        private async Task<List<ChatMessageModel>> BuildChatMessageModels(IList<ChatMessage> chatMessages)
        {
            HashSet<string> allNeededCmsUserMembers = new HashSet<string>();
            HashSet<string> allNeededCustomerMembers = new HashSet<string>();

            foreach (var message in chatMessages)
            {
                if (message.Sender.MemberType == ChatMemberType.CmsUser)
                {
                    allNeededCmsUserMembers.Add(message.Sender.MemberId);
                }
                else
                {
                    allNeededCustomerMembers.Add(message.Sender.MemberId);
                }
            }

            IList<CmsUser> cmsUser = await _dal.MarketData.CmsUser.Searcher.FindCmsUsers(
                ids: allNeededCmsUserMembers);
            IList<Customer> customers = await _dal.MarketData.Customer.Searcher.FindCustomers(
                ids: allNeededCustomerMembers);

            Dictionary<string, ChatMemberModel> allNeededMemberModels = new Dictionary<string, ChatMemberModel>(cmsUser.Count + customers.Count);

            foreach (var item in cmsUser)
            {
                allNeededMemberModels.Add($"{item.Id}_{ChatMemberType.CmsUser}", new ChatMemberModel
                {
                    MemberId = item.Id,
                    MemberType = ChatMemberType.CmsUser,
                    AvatarUrl = item.ProfileImageUrl,
                    NickName = item.Name
                });
            }

            foreach (var item in customers)
            {
                allNeededMemberModels.Add($"{item.Id}_{ChatMemberType.Customer}", new ChatMemberModel
                {
                    MemberId = item.Id,
                    MemberType = ChatMemberType.CmsUser,
                    AvatarUrl = item.AvatarUrl,
                    NickName = item.Username
                });
            }

            List<ChatMessageModel> mappedMessages = new List<ChatMessageModel>(chatMessages.Count);

            foreach (ChatMessage item in chatMessages)
            {
                var userKey = $"{item.Sender.MemberId}_{item.Sender.MemberType}";
                ChatMemberModel sender = allNeededMemberModels[userKey];

                mappedMessages.Add(
                   new ChatMessageModel
                   {
                       Id = item.Id,
                       ChannelId = item.ChannelId,
                       Sender = sender,
                       Timestamp = item.Timestamp,
                       Content = item.Content,
                       AttachedImageUrl = item.AttachedImageUrl
                   }
                );
            }

            return mappedMessages;
        }

        private OnlineUserModel MapUserConnectionToOnlineUserModel(UserConnections user)
        {
            return new OnlineUserModel
            {
                MemberId = user.User.MemberId,
                MemberType = user.User.MemberType,
                CmsUserType = user.User.CmsUserType,
                NickName = user.User.NickName,
                AvatarUrl = user.User.AvatarUrl,
                Device = user.Connections.Select(_ => _.Device).ToList().FirstOrDefault()
            };
        }

        #endregion
    }
}