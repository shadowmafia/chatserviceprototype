﻿using ChatService.Models.ChatData;
using DataAccessLayerAbstraction.ChatData.Mapping;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatService.Classes
{
    public interface IChatClient
    {
        Task MessageReceive(ChatMessageModel message);
        Task UserConnected(OnlineUserModel user);
        Task UsersDisconnected(List<OnlineUserModel> users);
        Task ErrorHandle(string message);
    }
}
