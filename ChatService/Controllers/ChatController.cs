﻿using ChatService.Classes;
using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ChatService.Controllers
{
    [ApiController]
    [Route("chatApi/v0/[controller]")]
    public class ChatController : ControllerBase
    {
        private readonly ChatBackgroundService _chatService;

        public ChatController(ChatBackgroundService chatService)
        {
            _chatService = chatService ?? throw new ArgumentNullException(nameof(chatService));
        }

        public async Task<IActionResult> CreateOrUpdateChannel(ChatChannel channel)
        {
            await _chatService.CreateOrUpdateChannel(channel);

            return Ok();
        }

        public async Task<IActionResult> AddMemberToChannel(string channelId, ChatMember member)
        {
            await _chatService.AddMemberToChannel(channelId, member);

            return Ok();
        }

        public async Task<IActionResult> RemoveMemberFromChannel(string channelId, ChatMember member)
        {
            await _chatService.RemoveMemberFromChannel(channelId, member);

            return Ok();
        }
    }
}