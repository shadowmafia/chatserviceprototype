﻿using ChatService.Classes;
using ChatService.Classes.Types;
using ChatService.Models.ChatData;
using ChatService.Models.ChatSystem;
using ChatService.Models.Settings;
using DataAccessLayerAbstraction.ChatData.Mapping;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChatService.Hubs
{
    public class ChatHub : Hub<IChatClient>
    {
        private readonly ChatBackgroundService _chatService;
        private readonly ChatHubSettings _settings;

        public ChatHub(ChatBackgroundService chatService, IOptions<ChatServiceSettings> settings)
        {
            _chatService = chatService ?? throw new ArgumentNullException(nameof(chatService));
            _settings = settings.Value.ChatHubSettings ?? throw new ArgumentNullException(nameof(settings.Value.ChatHubSettings));
        }

        //all users
        public override async Task OnConnectedAsync()
        {
            await _chatService.AddUserConnection(GetCurrentChatSub(), this.Context.ConnectionId, GetDevice());

            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            _chatService.RemoveUserConnection(GetCurrentChatSub(), Context.ConnectionId);

            await base.OnDisconnectedAsync(exception);
        }

        //Return all,lfg and last 15 private and 15 last public
        //[all authorized]
        public async Task<ChatUserChannelsModel> GetMyChannelsInfo(int? channelsLoadingCount = null)
        {
            if (channelsLoadingCount == null)
            {
                channelsLoadingCount = _settings.DefaultChannelTakeValue;
            }

            if (channelsLoadingCount.HasValue && channelsLoadingCount > _settings.MaxTakeChannelsValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(channelsLoadingCount)} cant be more than {_settings.MaxTakeChannelsValue}");
            }

            return await _chatService.GetChannelsInfo(GetCurrentChatSub(), channelsLoadingCount);
        }

        public async Task<List<ChatChannelModel>> GetMyChannelsInfoByType(ChatChannelType type, DateTime? elderThen = null, int? count = null)
        {
            if (count == null)
            {
                count = _settings.DefaultChannelTakeValue;
            }

            if (count > _settings.MaxTakeChannelsValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(count)} cant be more than {_settings.MaxTakeChannelsValue}");
            }

            return await _chatService.GetChannelsInfoByType(GetCurrentChatSub(), type, elderThen, count);
        }

        public async Task<ChatChannelModel> GetPrivateChannelWithMember(ChatMember receiver)
        {
            return await _chatService.GetPrivateChannelWithMember(GetCurrentChatSub(), receiver);
        }

        public async Task<ChatChannelModel> GetChannelInfo(string channelId)
        {
            return await _chatService.GetChannelInfo(GetCurrentChatSub(), channelId);
        }

        public async Task<List<ChatMessageModel>> GetMessagesFromChannel(string channelId, DateTime? elderThen = null, int? count = null)
        {
            if (count == null)
            {
                count = _settings.DefaultMessagesTakeValue;
            }

            if (count.Value > _settings.MaxTakeMessagesValue)
            {
                throw new ArgumentOutOfRangeException($"{nameof(count)} cant be more than {_settings.MaxTakeMessagesValue}");
            }

            return await _chatService.GetMessagesFromChannel(GetCurrentChatSub(), channelId, elderThen, count);
        }

        public List<OnlineUserModel> GetOnlineUsers()
        {
            return _chatService.GetOnlineUsers();
        }

        public async Task<ChatChannelModel> CreatePrivateChannelWithMember(ChatMember receiver)
        {
            return await _chatService.CreatePrivateChannelWithMember(GetCurrentChatSub(), receiver);
        }

        public async Task SendMessage(string channelId, string messageMarker, string content, string attachedImgUrl)
        {
            if (content.Length < _settings.MinMessageLength || content.Length > _settings.MaxMessageLength)
            {
                throw new ArgumentOutOfRangeException($"{nameof(content)} lenght cant be less than {_settings.MinMessageLength}");
            }

            var message = await _chatService.SendMessage(GetCurrentChatSub(), channelId, messageMarker, content, attachedImgUrl);

            await Clients.All.MessageReceive(message);
        }

        public async Task JoinToChannel(string channelId)
        {
            await _chatService.JoinToChannel(GetCurrentChatSub(), channelId);
        }

        public async Task LeaveFromChannel(string channelId)
        {
            await _chatService.LeaveFromChannel(GetCurrentChatSub(), channelId);
        }

        #region private methods

        private (string id, UserRoleTmp role) GetCurrentUserAuthInfoFromToken()
        {
            var splitedTokenInfo = Context.GetHttpContext().Request.Headers["Authorization"].ToString().Split("Bearer ")[1].Split("</separator>");
            string userId = splitedTokenInfo[0];
            UserRoleTmp role = Enum.Parse<UserRoleTmp>(splitedTokenInfo[1], ignoreCase: true);

            return (id: userId, role: role);
        }

        private (string id, UserRoleTmp role) GetCurrentUserAuthInfoFromHeaders()
        {
            var httpCtx = Context.GetHttpContext();
            string userId = httpCtx.Request.Headers["UserId"].ToString();
            UserRoleTmp role = Enum.Parse<UserRoleTmp>(httpCtx.Request.Headers["UserRole"], ignoreCase: true);

            return (id: userId, role: role);
        }

        private ChatSub GetCurrentChatSub()
        {
            var userAuthInfo = GetCurrentUserAuthInfoFromToken();

            string userId = userAuthInfo.id;
            UserRoleTmp role = userAuthInfo.role;

            return new ChatSub
            {
                Member = new ChatMember()//Todo: Get this user from auth service.
                {
                    MemberId = userId,
                    MemberType = role == UserRoleTmp.Customer ? ChatMemberType.Customer : ChatMemberType.CmsUser
                },
                RoleInSystem = role
            };
        }

        private DeviceType GetDevice()
        {
            var device = Context.GetHttpContext().Request.Headers["Device"].ToString();

            if (!string.IsNullOrEmpty(device))
            {
                if (device.Equals("Desktop"))
                {
                    return DeviceType.Desktop;
                }
                else if (device.Equals("Mobile"))
                {
                    return DeviceType.Mobile;
                }
                else
                {
                    return DeviceType.Web;
                }
            }

            return DeviceType.Web;
        }

        #endregion
    }
}