﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using ChatService.Classes;
using ChatService.Models.Settings;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ChatService.Hubs
{
    public class ChatHubRequestFilter : IHubFilter
    {
        private readonly ChatRequestFilterSettings _settings;
        private readonly ILogger<ChatHubRequestFilter> _logger;
        
        public ChatHubRequestFilter(ILogger<ChatHubRequestFilter> logger, IOptions<ChatServiceSettings> settings)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _settings = settings.Value.ChatRequestFilterSettings ?? throw new ArgumentNullException(nameof(settings.Value.ChatRequestFilterSettings));
        }

        public async ValueTask<object> InvokeMethodAsync(HubInvocationContext invocationContext, Func<HubInvocationContext, ValueTask<object>> next)
        {
            Stopwatch _timer = new Stopwatch();
            
            // var x = invocationContext;

            object resut;

            _timer.Start();

            try
            {
                resut = await next(invocationContext);
            }
            catch (Exception ex)
            {
                if (ex is ChatServiceException currentException)
                {
                    var currentLogText = $@"
                    __Chat hub exception
                    Time:{DateTime.UtcNow} 
                    Request url: {invocationContext.HubMethodName}
                    Error : {currentException.Message}";

                    if (currentException.IsPublic)
                    {

                    }

                    _logger.LogWarning(currentLogText);
                }
                else
                {
                    _logger.LogWarning(ex.Message);
                }

                if (ex.InnerException != null)
                {
                    _logger.LogWarning(ex.InnerException.Message);
                }

                throw;
            }

            _timer.Stop();

            if (_timer.ElapsedMilliseconds > _settings.RequestDurationForLoggingAsLargeInMs)
            {
                var name = string.Empty;
                _logger.LogWarning("Long Running Request: {Name} ({ElapsedMilliseconds} milliseconds) {@Request}", name, _timer.ElapsedMilliseconds, invocationContext);
            }

            return resut;
        }

        public Task OnConnectedAsync(HubLifetimeContext context, Func<HubLifetimeContext, Task> next)
        {
            return next(context);
        }

        public Task OnDisconnectedAsync(HubLifetimeContext context, Exception exception, Func<HubLifetimeContext, Exception, Task> next)
        {
            return next(context, exception);
        }
    }
}
