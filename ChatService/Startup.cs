using ChatService.Classes;
using ChatService.Hubs;
using ChatService.Models.Settings;
using DataAccessLayerImplementation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ChatService
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddElasticLoLBoostMarketDal(dalSettingsContainFile: "dataAccessLayerSettings.json");
            services.AddControllers();
            services.AddSignalR(hubOptions =>
            {
                hubOptions.AddFilter<ChatHubRequestFilter>();
            });

            AddChatBackgroundService(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "chatApi/v0/{controller}/{action}");
                endpoints.MapHub<ChatHub>("/chatHub");
            });
        }

        #region private methods

        private static void AddChatBackgroundService(IServiceCollection services)
        {
            IConfigurationSection chatServiceSettingsSection = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build().GetSection("ChatServiceSettings");
            services.Configure<ChatServiceSettings>(
                chatServiceSettingsSection
            );

            services.AddSingleton<ChatBackgroundService>();
        }

        #endregion
    }
}
