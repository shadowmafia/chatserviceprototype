﻿namespace ChatService.Models.Settings
{
    public class ChatServiceSettings
    {
        public ChatBackgroundServiceSettings ChatBackgroundServiceSettings { get; set; }
        public ChatHubSettings ChatHubSettings { get; set; }
        public ChatRequestFilterSettings ChatRequestFilterSettings { get; set; }
    }

    public class ChatBackgroundServiceSettings
    {
        public int DisconnectNotificationPeriodInSeconds { get; set; }
    }

    public class ChatRequestFilterSettings
    {
        public int RequestDurationForLoggingAsLargeInMs { get; set; }
    }

    public class ChatHubSettings
    {
        public int MaxTakeChannelsValue { get; set; }
        public int MaxTakeMessagesValue { get; set; }
        public int DefaultChannelTakeValue { get; set; }
        public int DefaultMessagesTakeValue { get; set; }
        public int MaxMessageLength { get; set; }
        public int MinMessageLength { get; set; }
    }
}
