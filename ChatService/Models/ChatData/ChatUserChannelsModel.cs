﻿using System.Collections.Generic;

namespace ChatService.Models.ChatData
{
    public class ChatUserChannelsModel
    {
        public List<ChatChannelModel> Channels { get; set; }
        public long TotalUnreadMessagesCount { get; set; }
    }
}
