﻿using ChatService.Classes.Types;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using DataAccessLayerAbstraction.MarketData.Mapping.CmsUserClasses;

namespace ChatService.Models.ChatData
{
    public class OnlineUserModel
    {
        public string MemberId { get; set; }
        public ChatMemberType MemberType { get; set; }
        public CmsUserType? CmsUserType { get; set; }
        public string NickName { get; set; }
        public string AvatarUrl { get; set; }
        public DeviceType Device { get; set; }
    }
}
