﻿using DataAccessLayerAbstraction.ChatData.Mapping.Models;
using System.Collections.Generic;

namespace ChatService.Models.ChatData
{
    public class ChatChannelModel
    {
        public string Id { get; set; }
        public string LinkedObjectId { get; set; }
        public ChatChannelType ChannelType { get; set; }
        public List<ChatMemberModel> ChannelMembers { get; set; }
        public ChatMessageModel LastMessage { get; set; }
        public long UnreadMessagesCount { get; set; }
    }
}
