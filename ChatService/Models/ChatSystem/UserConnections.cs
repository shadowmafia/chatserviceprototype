﻿using ChatService.Models.ChatData;
using System.Collections.Generic;

namespace ChatService.Models.ChatSystem
{
    public class UserConnections
    {
        public ChatMemberModel User { get; set; }
        public List<Connection> Connections { get; set; }
    }
}
