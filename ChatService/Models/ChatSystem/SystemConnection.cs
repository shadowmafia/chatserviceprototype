﻿namespace ChatService.Models.ChatSystem
{
    public class SystemConnection
    {
        public string SystemName { get; set; }
        public Connection Connection { get; set; }
    }
}
