﻿using ChatService.Classes.Types;
using DataAccessLayerAbstraction.ChatData.Mapping.Models;

namespace ChatService.Models.ChatSystem
{
    public class ChatSub
    {
        public ChatMember Member { get; set; }
        public UserRoleTmp RoleInSystem { get; set; }
    }
}
