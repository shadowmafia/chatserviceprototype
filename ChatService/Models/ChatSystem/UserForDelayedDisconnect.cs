﻿using ChatService.Models.ChatData;
using System;

namespace ChatService.Models.ChatSystem
{
    public class UserForDelayedDisconnect
    {
        public ChatMemberModel User { get; set; }
        public DateTime DisconnectTime { get; set; }
    }
}
