﻿using ChatService.Classes.Types;
using System;

namespace ChatService.Models.ChatSystem
{
    public class Connection
    {
        public string ConnectionId { get; set; }
        public DeviceType Device { get; set; }
        public DateTime ConnectionTime { get; set; }

    }
}
